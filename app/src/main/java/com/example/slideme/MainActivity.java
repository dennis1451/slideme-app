package com.example.slideme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN = 3000;
    Animation downAnim;
    TextView textLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        downAnim = AnimationUtils.loadAnimation(this, R.anim.down_animation);

        textLogo = findViewById(R.id.createText);

        textLogo.setAnimation(downAnim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, BtActivity.class);
                startActivity(intent);
                finish();
            }

        }, SPLASH_SCREEN);
    }
}