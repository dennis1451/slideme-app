package com.example.slideme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class EnglishActivity extends AppCompatActivity {

    TextView mTextViewCountDown, TextViewHebrew;
    Button mButtonStartPause, buttonReset;
    CountDownTimer mCountDownTimer;
    boolean mTimerRunning = false;
    SeekBar timeBar;

    private static final String TAG = "BlueTest5-Controlling";
    private int mMaxChars = 50000;//Default//change this to string..........
    private UUID mDeviceUUID;
    private BluetoothSocket mBTSocket;
    private EnglishActivity.ReadInput mReadThread = null;

    private boolean mIsUserInitiatedDisconnect = false;
    private boolean mIsBluetoothConnected = false;

    private BluetoothDevice mDevice;

    final static String on="a";//on
    final static String off="b";//off

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_english);

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel("FinishE","FinishE", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        TextViewHebrew = (TextView)findViewById(R.id.textViewHebrew);

        timeBar = findViewById(R.id.seekBarTime);
        mTextViewCountDown = findViewById(R.id.textTimer);
        mButtonStartPause = findViewById(R.id.button_start_pause);
        buttonReset = findViewById(R.id.button_reset);
        timeBar.setMax(1800);
        timeBar.setProgress(300);

        timeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                update(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ///////////////////////////
        ActivityHelper.initialize(this);


        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        mDevice = b.getParcelable(BtActivity.DEVICE_EXTRA);
        mDeviceUUID = UUID.fromString(b.getString(BtActivity.DEVICE_UUID));
        mMaxChars = b.getInt(BtActivity.BUFFER_SIZE);

        Log.d(TAG, "Ready");

    }

    public void click(View view) {

        if (mTimerRunning) {
            pause();
        } else {
            Start_Timer();
        }
    }

    public void Start_Timer() {

        try {
            mBTSocket.getOutputStream().write(on.getBytes());

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String a = mTextViewCountDown.getText().toString();
        String[] separated = a.split(":");
        String m = separated[0];
        int mInt = m.length();
        String s = separated[1];
        if(mInt==2){
            m=0+m;
        }
        String all = m+s;
        all = all.replaceAll("\\s+","");
        Log.d("maxim-time",all);

            timeBar.setEnabled(false);
            mCountDownTimer = new CountDownTimer(timeBar.getProgress() * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                update((int) millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    notification();
                    reset();
                }
            }.start();

            mTimerRunning = true;
            mButtonStartPause.setText("PAUSE");
            buttonReset.setVisibility(View.INVISIBLE);
    }

    private void reset() {
        mTextViewCountDown.setText("5 : 00");
        timeBar.setProgress(300);
        mCountDownTimer.cancel();
        mButtonStartPause.setText("START");
        timeBar.setEnabled(true);
        mTimerRunning = false;
        mButtonStartPause.setVisibility(View.VISIBLE);
        buttonReset.setVisibility(View.INVISIBLE);
    }

    private void pause() {
        try {
            mBTSocket.getOutputStream().write(off.getBytes());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mCountDownTimer.cancel();
        mTimerRunning = false;
        mButtonStartPause.setText("CONTINUE");
        buttonReset.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mTimerRunning){
            mCountDownTimer.cancel();
        }
    }

    private void update(int progress) {
        int minutes = progress / 60;
        int seconds = progress % 60;

        String secondsFinal = "";
        if(seconds <=9){
            secondsFinal ="0" + seconds;
        }
        else{
            secondsFinal ="" + seconds;
        }
        timeBar.setProgress(progress);
        mTextViewCountDown.setText("" + minutes +" "+ ":"+" " + secondsFinal );

    }

    public void stopClick(View view) {
        reset();
    }

    public void notification()
    {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(EnglishActivity.this,"FinishE");

        Intent intent = new Intent(this,EnglishActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,1,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentTitle("Slide Me");
        builder.setContentText("Test - Finish - English");
        builder.setSmallIcon(R.drawable.slideme_logo);
        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(EnglishActivity.this);
        notificationManagerCompat.notify(1,builder.build());
    }

    public void loadHebrew(View view) {

        Intent intent = new Intent(this,HebrewActivity.class);
        startActivity(intent);
    }
    private class ReadInput implements Runnable {

        private boolean bStop = false;
        private Thread t;

        public ReadInput() {
            t = new Thread(this, "Input Thread");
            t.start();
        }

        public boolean isRunning() {
            return t.isAlive();
        }

        @Override
        public void run() {
            InputStream inputStream;

            try {
                inputStream = mBTSocket.getInputStream();
                while (!bStop) {
                    byte[] buffer = new byte[256];
                    if (inputStream.available() > 0) {
                        inputStream.read(buffer);
                        int i = 0;
                        /*
                         * This is needed because new String(buffer) is taking the entire buffer i.e. 256 chars on Android 2.3.4 http://stackoverflow.com/a/8843462/1287554
                         */
                        for (i = 0; i < buffer.length && buffer[i] != 0; i++) {
                        }
                        final String strInput = new String(buffer, 0, i);

                        /*
                         * If checked then receive text, better design would probably be to stop thread if unchecked and free resources, but this is a quick fix
                         */



                    }
                    Thread.sleep(500);
                }
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        public void stop() {
            bStop = true;
        }

    }

    private class DisConnectBT extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {//cant inderstand these dotss

            if (mReadThread != null) {
                mReadThread.stop();
                while (mReadThread.isRunning())
                    ; // Wait until it stops
                mReadThread = null;

            }

            try {
                mBTSocket.close();
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mIsBluetoothConnected = false;
            if (mIsUserInitiatedDisconnect) {
                finish();
            }
        }

    }

    private void msg(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        if (mBTSocket != null && mIsBluetoothConnected) {
            new DisConnectBT().execute();
        }
        Log.d(TAG, "Paused");
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mBTSocket == null || !mIsBluetoothConnected) {
            new ConnectBT().execute();
        }
        Log.d(TAG, "Resumed");
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "Stopped");
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
// TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void> {
        private boolean mConnectSuccessful = true;

        @Override
        protected void onPreExecute() {

            progressDialog = ProgressDialog.show(EnglishActivity.this, "Hold on", "Connecting");// http://stackoverflow.com/a/11130220/1287554

        }

        @Override
        protected Void doInBackground(Void... devices) {

            try {
                if (mBTSocket == null || !mIsBluetoothConnected) {
                    mBTSocket = mDevice.createInsecureRfcommSocketToServiceRecord(mDeviceUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    mBTSocket.connect();
                }
            } catch (IOException e) {
                // Unable to connect to device`
                // e.printStackTrace();
                mConnectSuccessful = false;



            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!mConnectSuccessful) {
                Toast.makeText(getApplicationContext(), "Could not connect to device.Please turn on your Hardware", Toast.LENGTH_LONG).show();
                finish();
            } else {
                msg("Connected to device");
                mIsBluetoothConnected = true;
                mReadThread = new ReadInput(); // Kick off input reader
            }

            progressDialog.dismiss();
        }

    }
    //@Override
    //protected void onDestroy() {
    //    // TODO Auto-generated method stub
    //   super.onDestroy();
    //}

    ///////////////////////////////////
}

